<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\EmailClientResponse;
use App\Mail\EmailPersonalSales;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Registro;
use App\Models\StateHistory;
use App\User;
use Mail;
use Laracasts\Flash\Flash;

class RegisterController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		date_default_timezone_set('America/Lima');	
	}
	public function form()
	{			
		return view('admin.register.form');
	}
	public function create(Request $request)
	{						
		$user = Auth::user()->name;
		$tipo = $request->input('tipo', null);
		$productotexto = $request->input('productotexto', null);
		$detalle = $request->input('detalle', null);
		$caso = $request->input('caso', null);
		$ocompra = $request->input('ocompra', null);
		$opnum = $request->input('opnum', null);
		$cliente = $user;
		$estado = "REGISTRADO";
		$contacto = $request->input('contacto', null);
		$devolucion = $request->input('devolucion', null);
		$cargo_contacto = $request->input('cargo_contacto', null);
		$email_contacto = $request->input('email_contacto', null);
		$rbt = $request->input('customRadio', null);

			
		$fecha = date('Y-m-d');		
		
		$data['fecha'] = $fecha;
		if (!is_null($caso)) {
			$obj_register = new Registro();

			$data['tipo'] = $tipo;
			$data['productotexto'] = $productotexto;
			$data['detalle'] = $detalle;
			$data['caso'] = $caso;
			$data['ocompra'] = $ocompra;
			$data['opnum'] = $opnum;
			$data['cliente'] = $cliente;
			$data['estado'] = $estado;
			$data['cargo_contacto'] = $cargo_contacto;
			$data['email_contacto'] = $email_contacto;
			$data['contacto'] = $contacto;
			$data['afectado'] = $rbt;
			

			if (!is_null($devolucion)) {
				$data['devolucion'] = $devolucion;
			} else {
				$data['devolucion'] = "NO";
			}

			if ($request->hasFile('docajunto')) {
				$file = $request->file('docajunto');
				   //obtenemos el nombre del archivo
				$fechaD = strftime("%Y-%m-%d %H-%M-%S", time());
				$nombre = $file->getClientOriginalName();	
				$file_name = $fechaD . "-" . $nombre;						
				//$file_name = $nombre . "-" . $fecha;				
				   //indicamos que queremos guardar un nuevo archivo en el disco local
				\Storage::disk('attached')->put($file_name, \File::get($file));
				$data['docajunto'] = $file_name;
			} else {
				$file_name = "";
				$data['docajunto'] = $file_name;
			}

			$obj_register->create($data);            
			/*Guardar registro en state_history*/
			$user = Registro::all();
			$dat = $user->last();
			if (!is_null($dat)) {
				$id = $dat->idregistro;
				$id_user = Auth::id();
				$obj_history = new StateHistory();

				$obj_history->idregistro = $id;
				$obj_history->id_user = $id_user;
				$obj_history->date_register = $fecha;
				$obj_history->save();

			}
			/*fin registro*/
			/*MODIFICAR UNA VEZ EN EL SERVIDOR*/		
			$email_send = 
			[
				/*
				'as.sig@amfa.com.pe',
				'practicante.sig@amfa.com.pe',
				'cliente@amfa.com.pe',
				'ascomercial@amfa.com.pe',
				'j.sig@amfa.com.pe',*/	
				'rsalvador@monorperu.com',
				'alfredo16_89@outlook.com',			
			];	
			
			foreach ($email_send as $row) {				
				Mail::to($row)				
					->send(new EmailPersonalSales($data));				
			}

			//Mail::to('salvador8908@gmail.com')				
			//		->send(new EmailPersonalSales($data));	

			$message = "El registro se guardó de manera exitosa";
			$type = "success";
			Flash($message, $type);				
			return redirect()->route('home');
		}
	}

	public function formUpdate($id = null)
	{		
		if (!is_null($id)) {
			$register = Registro::find($id);			
			if (isset($register)) {
				$data['dataRegister'] = $register;
				return view('admin.register.form-update', $data);				
			} else {
				return view('error.404');
			}
		} 
	}	

	public function details($id = null)
	{		
		if (!is_null($id)) {
			$user_data = Registro::find($id);

			$data['data_user'] = $user_data;
			//dd($user_data);		
		return view('admin.register.details', $data);
		}		
	}

	public function update(Request $request)
	{
		$id = $request->input('id', null);
		$user = Auth::user()->name;
		$obj_register = Registro::find($id);
		$user = Auth::user()->name;
		$procede = $request->input('procede', null);
		$evaluacion = $request->input('evaluacion', null);
		$spvnum = $request->input('spvnum', null);
		$tipo = $request->input('tipo', null);		
		$ocompra = $request->input('ocompra', null);
		$email = $request->input('email_contacto', null);
		$fecha = date('Y-m-d');
		$cliente = Auth::user()->name;


		/*Reclamo, observacion, consulta, sugerencia */
        if (($tipo == "Reclamo") || ($tipo == "Observacion") || ($tipo == "Consulta") || ($tipo == "Sugerencia")) {
            if ((!is_null($spvnum)) && (is_null($procede) && (is_null($evaluacion))) ) {
                $estado = "RECEPCIONADO";
				
				$obj_register->spvnum = $spvnum;	
                $obj_register->estado = $estado;
                $obj_register->fechagenspv = $fecha;
                /*Insertado en la tabla historial*/
                $dat = [
                    'date_reception' => $fecha
                ];
                StateHistory::where('idregistro', '=', $id)->update($dat);
                /*-------------------------------------------*/
                $data =
                    [
                        'fecha' => $fecha,
                        'ocompra' => $ocompra,
                        'tipo' => $tipo,
                        'cliente' => $cliente,
                        'estado' => $estado,
                        'tipo' => $tipo,
                        'email_contacto' => $email
					];		

					$email_contacto = $email;
					
					$email_send = 					
					[
						/*
						$email_contacto,
						'as.sig@amfa.com.pe',
						'practicante.sig@amfa.com.pe',
						'cliente@amfa.com.pe',
						'ascomercial@amfa.com.pe',
						'j.sig@amfa.com.pe',*/	
						$email_contacto, 				
						'rsalvador@monorperu.com',
						'alfredo16_89@outlook.com',			
					];	
					
			foreach ($email_send as $row) {				
				Mail::to($row)				
					->send(new EmailClientResponse($data));								
			}			

                //Mail::to($data['email_contacto'])->send(new EmailClientResponse($data));
            }

            if (($procede == "SI") && (is_null($evaluacion))) {
                $estado = "EN EVALUACIÓN";
                $obj_register->procede = "SI";
                $obj_register->estado = $estado;
                /*Insertado en la tabla historial*/
                $dat = [
                    'date_evaluation' => $fecha
                ];
                StateHistory::where('idregistro', '=', $id)->update($dat);
                /**/
                $data =
                    [
                        'fecha' => $fecha,
                        'ocompra' => $ocompra,
                        'tipo' => $tipo,
                        'cliente' => $cliente,
                        'estado' => $estado,
                        'tipo' => $tipo,
                        'email_contacto' => $email
					];
				
					
					$email_contacto = $email;
					
					$email_send = 					
					[
						/*
						$email_contacto,
						'as.sig@amfa.com.pe',
						'practicante.sig@amfa.com.pe',
						'cliente@amfa.com.pe',
						'ascomercial@amfa.com.pe',
						'j.sig@amfa.com.pe',*/	
						$email_contacto, 				
						'rsalvador@monorperu.com',
						'alfredo16_89@outlook.com',			
					];	
					
			foreach ($email_send as $row) {				
				Mail::to($row)				
					->send(new EmailClientResponse($data));								
			}			


				//Mail::to($data['email_contacto'])->send(new EmailClientResponse($data));
				
            } elseif(($procede == "NO") && (is_null($evaluacion))) {
                $estado = "ATENDIDO";
                $obj_register->procede = "NO";
                $obj_register->estado = $estado;
                /*Insertado en la tabla historial*/
                $dat = [
                    'date_close' => $fecha
                ];
                StateHistory::where('idregistro', '=', $id)->update($dat);
                /**/
                $data =
                    [
                        'fecha' => $fecha,
                        'ocompra' => $ocompra,
                        'tipo' => $tipo,
                        'cliente' => $cliente,
                        'estado' => $estado,
                        'tipo' => $tipo,
                        'email_contacto' => $email
                    ];

					
					$email_contacto = $email;
					
					$email_send = 					
					[
						/*
						$email_contacto,
						'as.sig@amfa.com.pe',
						'practicante.sig@amfa.com.pe',
						'cliente@amfa.com.pe',
						'ascomercial@amfa.com.pe',
						'j.sig@amfa.com.pe',*/	
						$email_contacto, 				
						'rsalvador@monorperu.com',
						'alfredo16_89@outlook.com',			
					];	
					
			foreach ($email_send as $row) {				
				Mail::to($row)				
					->send(new EmailClientResponse($data));								
			}	
                //Mail::to($data['email_contacto'])->send(new EmailClientResponse($data));
            }

            if (!is_null($evaluacion)) {
				$estado = "ATENDIDO";				
				$obj_register->evaluacion = $evaluacion;
				if ($spvnum) {
					$obj_register->spvnum = $spvnum;
				}
                $obj_register->estado = $estado;

                /*Insertado en la tabla historial*/
                $dat = [
                    'date_close' => $fecha
                ];
                StateHistory::where('idregistro', '=', $id)->update($dat);
                /**/
                $data =
                    [
                        'fecha' => $fecha,
                        'ocompra' => $ocompra,
                        'tipo' => $tipo,
                        'cliente' => $cliente,
                        'estado' => $estado,
                        'tipo' => $tipo,
                        'email_contacto' => $email
					];
					
					
					$email_contacto = $email;
					
					$email_send = 					
					[
						/*
						$email_contacto,
						'as.sig@amfa.com.pe',
						'practicante.sig@amfa.com.pe',
						'cliente@amfa.com.pe',
						'ascomercial@amfa.com.pe',
						'j.sig@amfa.com.pe',*/	
						$email_contacto, 				
						'rsalvador@monorperu.com',
						'alfredo16_89@outlook.com',			
					];	
					
			foreach ($email_send as $row) {				
				Mail::to($row)				
					->send(new EmailClientResponse($data));								
			}			


                //Mail::to($data['email_contacto'])->send(new EmailClientResponse($data));
            }

            if ($request->hasFile('rutaspv')) {
                $file = $request->file('rutaspv');
                //obtenemos el nombre del archivo
                $fechaD = strftime("%Y-%m-%d %H-%M-%S", time());
                $nombre = $file->getClientOriginalName();
                $name_file = $fechaD."-".$nombre;
                //indicamos que queremos guardar un nuevo archivo en el disco local
                \Storage::disk('spv')->put($name_file, \File::get($file));
                $obj_register->rutaspv = $name_file;
            } else {
                $name_file = "";
                $obj_register->rutaspv = $name_file;
            }

            $obj_register->update();
            $message = "El registro se modifico de manera exitosa";
            $type = "success";
            Flash($message, $type);
            return redirect()->route('home');
        }
        /*Fin Reclamo*/
	}

	public function deleteRegister($id = null)
	{		
		//$id = json_decode(stripslashes($_GET['id']));			
		if (!is_null($id)) {
			$delete_register = Registro::find($id);
			//dd($delete_user);
			if (!is_null($delete_register)) {				
				if ($delete_register->docajunto != "") {
					if(file_exists(public_path('documentos/file-adjunto/'. $delete_register->docajunto))) {
						unlink(public_path('documentos/file-adjunto/'. $delete_register->docajunto));
					}
				}
								
				$delete_register->delete();	
				
				$message = "El registro se eliminó de manera correcta.";
				$type = "info";
				Flash($message, $type);			
				return redirect()->route('home');
			} else {
				$message = "Upps!! ocurrió un error inesperado al intentar eliminar el usuario.";
				$type = "warning";
				Flash($message, $type);
				return redirect()->route('home');
			}
		}
	}	
	/*
	public function delete($id = null)
	{
		if (!is_null($id)) {
			$register = Registro::find($id);

			if (!is_null($register)) {
				$register->delete();

				$message = "El registro se eliminó de manera exitosa";
				$type = "success";
				Flash($message, $type);
				return redirect()->route('home');
			} else {
				$message = "Upps!! hubo un error al intentar eliminar el registro";
				$type = "error";
				Flash($message, $type);
				return redirect()->route('home');
			}
		}
	}*/
}
