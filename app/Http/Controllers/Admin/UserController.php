<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class UserController extends Controller
{


	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$data['user'] = DB::table('users')->orderBy('id', 'desc')->get();
		return view('admin.users.index', $data);
	}

	public function form()
	{

		return view('admin.users.form');
	}

	public function dataUpdate($id = null)
	{
		if (!is_null($id)) {
			$user = User::find($id);
			if (!is_null($user)) {
				$data['user'] = $user;
				return view('admin.users.form-update', $data);
			} else {
				return view('error.404');
			}
		}
	}

	public function create(Request $request)
	{
		$user = $request->input('user_name', null);		
		$email = $request->input('email', null);
		$ruc = $request->input('ruc', null);
		$password = $request->input('password', null);		
		$type_user = $request->input('type_user', null);

		if (!is_null($user)) {			
				$obj_user = new User();

				$data['name'] = $user;
				$data['email'] = $email;
				$data['ruc'] = $ruc;
				$data['password'] = Hash::make($password);
				$data['tipoUsuario'] = $type_user;

				$user = $obj_user->create($data);								

				$message = "Se registró el usuario de manera correcta.";
				$type = "success";
				Flash($message, $type);
				return redirect()->route('admin.user.form');			
		} else {
			$message = "Error al registrar al usuario, al parecer el campo esta vacio.";
			$type = "warning";
			Flash($message, $type);
			return redirect()->route('admin.user.form');
		}
	}

	public function update(Request $request)
	{
		$id = $request->input('id', null);
		$user = $request->input('user_name', null);
		$email = $request->input('email', null);
		$ruc = $request->input('ruc', null);
		$type_user = $request->input('type_user', null);
		$password = $request->input('password', null);

		if (!is_null($id)) {
			$user_data = User::find($id);
			if (!is_null($user)) {
				$user_data->name = $user;
				$user_data->email = $email;
				$user_data->tipoUsuario = $type_user;
				if (!is_null($ruc)) {
					$user_data->ruc = $ruc;
				}
				if (!is_null($password)) {					
					$user_data->password = Hash::make($password);					
				}

				$user_data->update();				
				$message = "El usuario se actualizó de manera exitosa.!!!";
				$type = "success";
				Flash($message, $type);
				return redirect()->route('admin.user.index');
			} else {
				$message = "Upps!! ocurrió un error inesperado al tratar de actualizar los datos del usuario.";
				$type = "warning";
				Flash($message, $type);
				return redirect()->route('admin.user.index');
			}
		}

	}

	public function delete($id)
	{
		try {
			if (!is_null($id)) {
				$user = User::find($id);
				if (!is_null($user)) {
					$user->delete();
					$message = "El usuario fue eliminado.!!!";
					$type = "info";
					Flash($message, $type);				
					return redirect()->route('admin.user.index');
				}
			}
		} catch (Exception $ex) {
			$ex->getMessage();
		}
	}
}
