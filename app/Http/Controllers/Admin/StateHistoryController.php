<?php
namespace App\Http\Controllers\Admin;

use App\Models\StateHistory;
use App\Http\Controllers\Controller;
use DB;

class StateHistoryController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

     public function index()
    {
        $data['history'] = DB::select('SELECT u.name, h.id_user, h.date_register, h.date_reception, h.date_evaluation, h.date_close, h.idregistro, h.id
									FROM users u
									INNER JOIN state_history h on u.id = h.id_user
									ORDER BY id desc;');  
           
        return view('admin.state-history.index', $data);
    }
}