<?php
namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use DB;

class CommentController extends Controller
{
   

    public function create(Request $request)
    {
        $id_registro = $request->input('id_registro', null);
        $id_client = $request->input('id_client', null);    
        $client = $request->input('client', null);              
        $comment = $request->input('comment', null);

        $date = date('Y-m-d');        

        if (!is_null($id_registro)) {
            $obj_comment = new Comment();

            $data['idregistro'] = $id_registro;
            $data['id_client'] = $id_client;
            $data['client'] = $client;
            $data['comment'] = $comment;
            $data['date_comment'] = $date;

            $obj_comment->create($data);
            $message = "El registró su comentario.";
            $type = "success";
            Flash($message, $type);			
            return redirect()->route('home');
        }
    }

    public function statusChange()
    {
        $id = json_decode(stripslashes($_GET['id']));
        if (!is_null($id)) {
            $data_user = Comment::find($id);
            if (!is_null($data_user)) {
                $data_user->flagactive = 1;
                $data_user->update();

                return $id;
            }
        } 
    }

    public function form()
    {
        $data['comment_all'] = Comment::all();
        return view('admin.comment.form', $data);
    }

    public function delete()
    {
        $id = json_decode(stripslashes($_GET['id']));
        if (!is_null($id)) {
            $comment_data = Comment::find($id);
            if (!is_null($comment_data)) {
                $comment_data->delete();
                return $id;
            }
        }
    }
}
