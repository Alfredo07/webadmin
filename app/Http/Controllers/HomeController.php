<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registro;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        //$count_comments = DB::select('comments_client')->get();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      
        $comment_client = DB::table('comments_client')
                           ->where('flagactive', '=', '0')
                           ->get();
        $data['comment'] = $comment_client; 
                
        $comments = Comment::where('flagactive', '=', '0')->get();  
        $data['count'] = count($comments);        
        
        if ((Auth::user()->tipoUsuario == 1) || (Auth::user()->tipoUsuario == 2)) {
            $data['listRegister'] = Registro::all();          
        } else {
            $data['listRegister'] = DB::table('registro')
                                     ->where('cliente', '=', auth()->user()->name)
                                     ->orderBy('idregistro', 'desc')
                                     ->get();          
        }             
        return view('home', $data);
    }    
  
}
