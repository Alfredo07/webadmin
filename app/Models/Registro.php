<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    const TABLE_NAME = 'registro';
    protected $table = self::TABLE_NAME;
    protected $primaryKey = 'idregistro';
    protected $fillable =
        [
        'idregistro',
        'tipo',
        'devolucion',
        'persiste',
        'ocompra',
        'productocod',
        'productotexto',
        'nguia',
        'detalle',
        'fechareg',
        'fechagenspv',
        'estado',
        'registra',
        'docajunto',
        'cliente',
        'caso',
        'opnum',
        'spvnum',
        'procede',
        'evaluacion',
        'rutaspv',
		'contacto',
        'cargo_contacto',
        'email_contacto',
        'afectado'
    ];

    public function list($id)
    {
        return $users = DB::table('registro')
            ->where('idregistro', '=', $id)
            ->get();
    }


}