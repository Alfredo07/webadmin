<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class StateHistory extends Model
{
    const TABLE_NAME = 'state_history';
    protected $table = self::TABLE_NAME;
}