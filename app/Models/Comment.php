<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Comment extends Model
{
    const TABLE_NAME = "comments_client";

    protected $table = self::TABLE_NAME;
    protected $fillable =
    [
        'id',
        'id_client',
        'idregistro',
        'client',
        'comment',
        'date_comment', 
        'flagactive',       
    ];
}
