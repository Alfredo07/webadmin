<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
	const TABLE_NAME = 'product';

	protected $table = self::TABLE_NAME;
	protected $fillable = ['id', 'name', 'price', 'description', 'flagactive'];

	public function listAll()
	{
		return DB::table(self::TABLE_NAME)->get();
	}
}