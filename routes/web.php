<?php

Route::get('/', function () {
    return view('auth/login');   
});

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

/*Registro Controller */
Route::post('registerCreate', 'Admin\RegisterController@create')->name('admin.register.create');
Route::get('/form', 'Admin\RegisterController@form')->name('admin.register.form');
Route::get('/formUpdate/{id?}', 'Admin\RegisterController@formUpdate')->name('admin.user.formUpdate');
Route::post('/update/{id?}', 'Admin\RegisterController@update')->name('admin.register.update');
Route::get('/details/{id?}', 'Admin\RegisterController@details')->name('admin.register.details');
Route::get('deleteReg/{id?}', 'Admin\RegisterController@deleteRegister')->name('admin.register.delete');
/**/

/*Registro de usuarios*/
Route::get('/user', 'Admin\UserController@index')->name('admin.user.index');
Route::get('formUser', 'Admin\UserController@form')->name('admin.user.form');
Route::post('create', 'Admin\UserController@create')->name('admin.user.create');
Route::get('dataUpdate/{id?}', 'Admin\UserController@dataUpdate')->name('admin.user.dataUpdate');
Route::get('user/update', 'Admin\UserController@update')->name('admin.user.update');
Route::get('/delete/{id?}', 'Admin\UserController@delete')->name('admin.user.delete');
/**/

/**Comentarios */
Route::post('comment/create', 'Admin\CommentController@create')->name('comment.create');
Route::get('comment/index', 'Admin\CommentController@index')->name('comment.index');
Route::get('comment/status', 'Admin\CommentController@statusChange')->name('comment.status');
Route::get('comment/delete', 'Admin\CommentController@delete')->name('comment.delete');
Route::get('comment/form', 'Admin\CommentController@form')->name('comment.form');
/*Historial de Reclamos*/
Route::get('history', 'Admin\StateHistoryController@index')->name('history.index');
/**/

