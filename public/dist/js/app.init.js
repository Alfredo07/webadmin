$(function() {
  'use strict';
  $('#main-wrapper').AdminSettings({
    Theme: false, // this can be true or false ( true means dark and false means light ),
    Layout: 'vertical',
    LogoBg: 'skin6', // aqui modifico alfredo (skin5 -> deberia ser)
    NavbarBg: 'skin6', // aqui modifico alfredo (skin6 -> deberia ser)
    SidebarType: 'full', // You can change it full / mini-sidebar / iconbar / overlay
    SidebarColor: 'skin5', // aqui modifico alfredo (skin5 -> deberia ser)  // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
    SidebarPosition: true, // it can be true / false ( true means Fixed and false means absolute )
    HeaderPosition: true, // it can be true / false ( true means Fixed and false means absolute )
    BoxedLayout: false // it can be true / false ( true means Boxed and false means Fluid )
  });
});
