@extends('layouts.app')

@section('script-custom')
    <script src="{{ asset('public/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('public/dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('public/dist/js/app-style-switcher.js') }}"></script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(4000);

        function notification(id) {
            swal({
                title: "Estás seguro?",
                text: "Una vez eliminado, no podrá recuperar este registro!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        //swal("Upps! El usuario ha sido eliminado!", {
                        //icon: "success",
                        //});
                        window.location.href = "{{ route('admin.register.delete') }}/" + id;
                        //deleteAjax(id);
                    } else {
                        swal("El registro esta seguro!");
                    }
                });
        }

    </script>
    <script>
        function statusChange(id)
        {            
            var chk = document.getElementById('customCheck1').checked ;
            if (chk) {                
                $.ajax({        
                    type: "GET",           
                    url: '{{ route("comment.status") }}',
                    data: { id: id },
                    success: function(result) {   
                        //location.reload();                     
                        console.log(result);                            
                    },
                    error: function(){
                        console.log("ERROR AL PROCESAR LA DATA");
                    }    
                });
                
            } 
        }    
    </script>
@endsection

@section('content')
    <!--test-->
    <div class="row">
        <div class="col">
        </div>
        <div class="col-6">
        </div>
        <div class="col">              
            <p class="text-justify">
                  <p class="text-success" style="padding: ">Cómo registrar una incidencia?</p> 
            </p>      
        </div>                    
    </div>
    <div class="row">
        <div class="col">
            <a href="{{ route('admin.register.form') }}"><span class="btn btn-rounded btn-block btn-success">Nuevo</span></a>
        </div>
        <div class="col-9">
            @if(Auth::user()->tipoUsuario == 1)
                <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Visualizar comentarios">
                    <img src="{{ asset('public/assets/images/comentario.png') }}" data-toggle="modal" data-target=".long-modal" width="40" height="40"><span class="badge badge-danger">{{ (($count) > 0 ? $count : '') }}</span>                    
                </a>
            @endif
        </div>
        <div class="col">             
            <a href="{{ asset('public/documentos/pdf-reclamos.pdf') }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Comunicado" target="_blank"><img src="{{ asset('public/assets/images/pdf.png') }}" width="40" height="40"></a>
            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Tutorial de como registrar una incidencia." href="#"><img src="{{ asset('public/assets/images/video.png') }}" width="40" height="40" data-toggle="modal" data-target="#modal-delete"></a>
        </div>
    </div>  
    <br>
    <!---->
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Historial de Registros</h4>
                    <h6 class="card-subtitle">Este es el listado de todas las incidencias registradas por el usuario actual.</h6>                    
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>FECHA</th>
                                @if(Auth::user()->tipoUsuario == 1)
                                    <th class="text-center">CODIGO SPV</th>
                                @endif
                                <th>CLIENTE</th>
                                <th>ASUNTO</th>
                                <th>TIPO</th>
                                <th>PROCEDE</th>
                                <th>ESTADO</th>
                                <th>ADJUNTO</th>
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($listRegister)
                                @foreach($listRegister as $row)
                                    <tr>
                                        <td>{{ $row->idregistro }}</td>
                                        <td>{{ $row->created_at }}</td>
                                        @if(Auth::user()->tipoUsuario == 1)
                                            <td>{{$row->spvnum }}</td>
                                        @endif
                                        <td>{{ $row->cliente }}</td>
                                        <td>{{ $row->caso }}</td>
                                        <td>{{ $row->tipo }}</td>
                                        <td>{{ $row->procede }}</td>                                        
                                        <td>
                                            @if($row->estado == "RECEPCIONADO")
                                                <span class="badge badge-success">{{ $row->estado }}</span>
                                            @elseif($row->estado == "EN EVALUACIÓN")
                                                <span class="badge badge-primary">{{ $row->estado }}</span>
                                            @elseif($row->estado == "REGISTRADO")
                                                <span class="badge badge-info">{{ $row->estado }}</span>
                                            @elseif($row->estado == "ATENDIDO")
                                                <span class="badge badge-danger">{{ $row->estado }}</span>
                                            @endif    
                                        </td>                                        
                                        <td><a target="_blank" href="{{ asset('public/documentos/file-adjunto/'.$row->docajunto) }}">{{ $row->docajunto }}</a></td>
                                        
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-rounded btn-success dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu animated lightSpeedIn">
                                                    <a class="dropdown-item" href="{{ route('admin.register.details',$row->idregistro)}}">Ver
                                                        Detalle</a>
                                                    <!--********************************************-->
                                                    @if((Auth::user()->tipoUsuario == 1) || (Auth::user()->tipoUsuario == 2))
                                                        <a class="dropdown-item"
                                                           href="{{ route('admin.user.formUpdate',$row->idregistro)}}">Modificar</a>
                                                        <a class="dropdown-item" href="#"
                                                           onclick="notification({{ $row->idregistro }})">Eliminar</a>
                                                    @endif
                                                    <!--********************************************-->
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- order table -->
    <!--Modal Video-->
    <div id="modal-delete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card-header bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="m-b-0 text-white">Como registrar una incidencia en el portal</h4>
                </div>
                <div class="card-body">
                <!--<video src="{{ asset('public/documentos/videos/video.mp4') }}" width="750"
                           height="360" controls autoplay></video>-->
                    <iframe width="750" height="370" src="https://www.youtube.com/embed/jO1cn1KrLII" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--Fin Modal video-->
    
    <!--Modalcomentario-->
                               
                <div class="modal fade long-modal" tabindex="-1" role="dialog" aria-labelledby="longmodal" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="longmodal">Comentarios realizado por los clientes</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="card">
                                    <div class="card-body">
                                    <h4 class="card-title">Listado de comentarios</h4>                                        
                                        <ul class="list-unstyled m-t-40">
                                           @if(!is_null($comment))
                                           @foreach($comment as $row)
                                           <li class="media">
                                                <img src="{{ asset('public/assets/images/comentario.png') }}" width="40" heigth="40" alt="Generic placeholder image">                                                
                                                <div class="media-body">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <h5 class="mt-0 mb-1">{{ $row->client }}  
                                                    </div>
                                                    <div class="col-sm-2">                                                      
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="" id="customCheck1" onclick="statusChange({{ $row->id }});">
                                                        </label>
                                                    </fieldset>                                                  
                                                    </div>
                                                </div>
                                                <span class="card-subtitle">{{ $row->date_comment }}</span></h5>                                                                                                                                        
                                                    </br>
                                                    {{ $row->comment }}                                                    
                                                </div>
                                            </li>
                                            <hr>  
                                            @endforeach
                                           @endif                                                                             
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                            </div>
                        </div>                        
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->                               
    <!--Fin Modal-->
@endsection
