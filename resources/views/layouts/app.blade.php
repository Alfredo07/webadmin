<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/assets/images/logo-icon.png') }}">
    <title>Amfa Vitrum</title>
    <!-- Custom CSS -->
    <link href="{{ asset('public/assets/libs/chartist/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/extra-libs/c3/c3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('public/dist/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/libs/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    @yield('style-custom')  
    <script type="text/javascript">              
            function onload() {
                console.log("hola");
                $.ajax({
                        type: "GET",
                        url: "{{ route('comment.index') }}",
                        //data: "{empid: empid}",
                        contentType: "application/json; charset=utf-8",
                        //dataType: "json",
                        success: function(result) {
                            console.log(result);
                        }
                });
            }            
</script> 

</head>

<body>
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-theme="light" data-navbarbg="skin6" data-layout="vertical" data-sidebartype="full" data-sidebar-position="fixed" data-boxed-layout="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-logobg="skin6">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">

            <div class="navbar-header" data-logobg="skin6">
                <!-- This is for the sidebar toggle which is visible on mobile only -->
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                    <i class="ti-menu ti-close"></i>
                </a>
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <a class="navbar-brand" href="{{ route('home') }}">
                    <!-- Logo icon -->
                    <b class="logo-icon">
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->

                        <img src="{{ asset('public/assets/images/logo-icon.png') }}" alt="homepage" class="dark-logo"/> <!--
                            <img src="{{ asset('public/assets/images/logo-light-icon.png') }}" alt="homepage" class="light-logo" />
                            -->
                    </b>

                    <!-- Logo text -->

                    <span class="logo-text">
                           
                            <img src="{{ asset('public/assets/images/logo-text.png') }}" alt="homepage" class="dark-logo"/>
                            
                            <img src="{{ asset('public/assets/images/logo-light-text.png') }}" class="light-logo" alt="homepage"/>
                        </span>
                </a>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Toggle which is visible on mobile only -->
                <!-- ============================================================== -->
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                   aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="ti-more"></i>
                </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-left mr-auto">
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                            <i class="sl-icon-menu font-20"></i>
                        </a>
                    </li>                    
                    <!--                                   
                    <li class="nav-item dropdown">                      
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-comments font-20"></i>
                        </a>
                        <div class="dropdown-menu mailbox animated bounceInDown">
                            <span class="with-arrow">
                                <span class="bg-primary"></span>
                            </span>
                            <ul class="list-style-none">
                                <li>
                                    <div class="drop-title bg-primary text-white">
                                        <h4 class="m-b-0 m-t-5">4 Nuevos</h4>
                                        <span class="font-light">Comentarios</span>                                            
                                    </div>
                                </li>
                                <li>
                                    <div class="message-center notifications">
                                        
                                        <a href="#" onclick="onload()" class="message-item">
                                            <span class="btn btn-success btn-circle">
                                                <i class="ti-user"></i>
                                            </span>
                                            <div class="mail-contnet">
                                                <h5 class="message-title">Luanch Admin</h5>
                                                <span class="mail-desc">Just see the my new admin!</span>
                                                <span class="time">9:30 AM</span>
                                            </div>
                                        </a>
                                                                                   
                                    </div>
                                </li>                                   
                            </ul>
                        </div>                          
                    </li>
                    -->
                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar ps-container ps-theme-default ps-active-y">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li>
                        <div class="user-profile dropdown m-t-20">
                            <div class="user-pic">
                                <img src="{{ asset('public/assets/images/users/avatar.png') }}" width="65" height="64" alt="users" class="rounded-circle img-fluid"/>
                            </div>
                            <div class="user-content hide-menu m-t-10">
                                <h5 class="m-b-10 user-name font-medium">{{ Auth::user()->name }}</h5>
                                <a href="javascript:void(0)" class="btn btn-circle btn-sm m-r-5" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ti-settings"></i>
                                </a>
                                <a href="{{ route('logout') }}" title="Logout" class="btn btn-circle btn-sm">
                                    <i class="ti-power-off"></i>
                                </a>
                                <div class="dropdown-menu animated flipInY" aria-labelledby="Userdd">
                                    <!--<a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>-->
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}">
                                        <i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- User Profile-->
                    <li class="nav-small-cap">
                        <i class="mdi mdi-dots-horizontal"></i>
                        <span class="hide-menu">Clientes</span> 
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Car-Wheel"></i>
                            <span class="hide-menu">Registros </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{ route('home') }}" class="sidebar-link">
                                    <i class="icon-Record"></i>
                                    <span class="hide-menu">Listado</span>
                                </a>
                            </li>
                            @if(Auth::user()->tipoUsuario == 1)
                            <li class="sidebar-item">
                                    <a href="{{ route('comment.form') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu">Comentarios</span>
                                    </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    <!--solo administradores-->  
                    @if(Auth::user()->tipoUsuario != 0)                  
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Administración del Portal</span>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                <i class="icon-Mailbox-Empty"></i>
                                <span class="hide-menu">Usuarios </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                            @if((Auth::user()->tipoUsuario == 1) || (Auth::user()->tipoUsuario == 2) )
                                <li class="sidebar-item">
                                    <a href="{{ route('history.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu">Historial de los Registros</span>
                                    </a>
                                </li>
                            @endif  
                            @if(Auth::user()->tipoUsuario == 2)
                                <li class="sidebar-item">
                                    <a href="{{ route('admin.user.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu"> Registrar Usuario </span>
                                    </a>
                                </li> 
                            @endif                               
                            </ul>
                        </li>  
                    @endif 
                    <!--solo administradores-->                       
                </ul>
            </nav>
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <div class="container-fluid">
            @yield('content')
        </div>

        <footer class="footer text-center">
            All Rights Reserved by Amfa Vitrum S.A. Designed and Developed by
            <a href="#">Alfredo Salvador</a>.
        </footer>
    </div>
</div>
<!-- ============================================================== -->


<script src="{{ asset('public/assets/libs/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('public//assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('public/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- apps -->
<script src="{{ asset('public/dist/js/app.min.js') }}"></script>
<script src="{{ asset('public/dist/js/app.init.js') }}"></script>
<script src="{{ asset('public/dist/js/app-style-switcher.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('public/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('public/assets/extra-libs/sparkline/sparkline.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('public/dist/js/waves.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('public/dist/js/sidebarmenu.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('public/dist/js/custom.min.js') }}"></script>
<!--This page JavaScript -->
<!--chartis chart-->
<script src="{{ asset('public/assets/libs/chartist/dist/chartist.min.js') }}"></script>
<script src="{{ asset('public/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
<!--c3 charts -->
<script src="{{ asset('public/assets/extra-libs/c3/d3.min.js') }}"></script>
<script src="{{ asset('public/assets/extra-libs/c3/c3.min.js') }}"></script>
<!--chartjs -->
<script src="{{ asset('public/assets/libs/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('public/assets/libs/morris.js/morris.min.js') }}"></script>
<script src="{{ asset('public/dist/js/pages/dashboards/dashboard1.js') }}"></script>
<script src="{{ asset('public/assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('public/assets/libs/sweetalert2/sweet-alert.init.js') }}"></script>


@yield('script-custom')
</body>

</html>
