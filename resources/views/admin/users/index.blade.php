@extends('layouts.app')

@section('script-custom')
<script src="{{ asset('public/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('public/dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(4000);
</script>
<script>
    function notification(id)
    {
        swal({
            title: "Estás seguro?",
            text: "Una vez eliminado, no podrá recuperar este usuario!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                //swal("Upps! El usuario ha sido eliminado!", {
                //icon: "success",
                //});
                window.location.href = '{{ route("admin.user.delete") }}/' + id;
            } else {
                swal("El usuario esta seguro!");
            }
            });
    }
</script>
@endsection

@section('content')
<!-- order table -->
<div class="row">
    <div class="col-12">
        @include('flash::message')
         <div class="col-sm-2">            
            <a href="{{ route('admin.user.form') }}"><span class="btn btn-rounded btn-block btn-success">Nuevo Usuario</span></a>            
        </div>
          <br>
         <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Listado de usuarios</h4>
                    <br>
        <div class="table-responsive">
            <table id="default_order" class="table table-hover table-striped table-bordered display" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>CORREO</th>
                        <th>TIPO USUARIO</th>
                        <th>RUC</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $row)
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <!--********************************************-->
                        <td>
                            @if($row->tipoUsuario == 0)
                                <span>Usuario</span>
                            @elseif($row->tipoUsuario == 1)
                                <span>Administrador</span>
                            @elseif($row->tipoUsuario == 2)
                                <span>Super-administrador</span>
                            @endif
                        </td>
                        <!--********************************************-->
                        <td>{{ $row->ruc }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-rounded btn-success dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Accion
                                </button>
                                <div class="dropdown-menu animated lightSpeedIn">                                                             
                                        <a class="dropdown-item"
                                           href="{{ route('admin.user.dataUpdate', $row->id) }}">Modificar</a>
                                        <a class="dropdown-item" href="#"
                                           onclick="notification({{ $row->id }})">Eliminar</a>                                
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>                
            </table>
        </div>
</div>
</div>

    </div>
</div>

@endsection
