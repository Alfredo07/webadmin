@extends('layouts.app')
@section('style-custom')
    <link rel="stylesheet" href="{{ asset('public/assets/lib/jquery-validate/error.css') }}">
@endsection

@section('script-custom')
    <script src="{{ asset('public/assets/lib/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/lib/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script>
        $ (document).ready(function () {
            $ ("#frmUpdate").validate({

                rules: {
                    user_name: {
                        required: true,
                        minlength: 4,
                        maxlength: 100
                    },
                    email: {
                        required: true,
                        email:true,
                        minlength: 6,
                        maxlength: 100
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 100
                        //maxlength: 9,
                        //digits: true
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#password"
                    },
                },
                messages: {
                    user_name: {
                        required: "Este campo es obligatorio",
                        minlength: "Como minimo este campo debe contener 4 caracteres"
                    },
                    email: {
                        required: "Este campo es obligatorio",
                        email: "Por favor ingrese un email válido",
                        minlength: "Como mínimo este campo debe contener 4 caracteres"
                    },
                    password: {
                        required: "Por favor ingrese su contraseña",
                        minlength: "Por su seguridad este campo no debe contener menos de 8 caracteres"
                    },
                    confirm_password: {
                        required: "Por favor ingrese la confirmación de su contraseña",
                        equalTo: "La contraseña no coincide"
                    },
                },
            });
        });
    </script>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-body">
            <h4 class="card-title">Actualizar los datos del usuario</h4>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    {!!Form::open(['method' => 'GET', 'id' => 'frmUpdate', 'role' => 'form', 'route' => 'admin.user.update', 'enctype' => 'multipart/form-data'])!!}
                    {{ csrf_field() }}
                    @if($user)
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="form-group">
                        <label for="exampleInputEmail111">Nombre Usuario</label>
                        <input type="text" class="form-control" maxlength="35" id="user_name" name="user_name" value="{{ $user->name }}" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail12">Correo Electrónico</label>
                        <input type="email" class="form-control" id="email" maxlength="45" name="email" placeholder="" value="{{ $user->email }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword11">Ruc</label>
                        <input type="text" class="form-control" id="ruc" name="ruc" maxlength="11" placeholder="" value="{{ $user->ruc }}">
                    </div>
                    <!--********************************************-->
                    <div class="form-group">
                        <label for="exampleInputEmail111">Tipo de Usuario</label>
                        @if($user->tipoUsuario == 0)
                            <select class="custom-select" id="type_user" name="type_user">                                
                                <option value="0">Cliente</option>
                                <option value="1">Administrador</option>
                                <option value="2">Super-administrador</option>
                            </select>
                        @elseif($user->tipoUsuario == 1)
                            <select class="custom-select" id="type_user" name="type_user">                                                                     
                                    <option value="1">Administrador</option>
                                    <option value="2">Super-administrador</option>
                                    <option value="0">Cliente</option>
                            </select>
                        @elseif($user->tipoUsuario == 2)
                            <select class="custom-select" id="type_user" name="type_user"> 
                                    <option value="2">Super-administrador</option> 
                                    <option value="0">Cliente</option>
                                    <option value="1">Administrador</option>
                            </select>
                        @endif
                    </div>                                        
                    <!--********************************************-->
                    @endif
                    <!---->
                    <div class="form-group">
                    <p>
                        <a class="btn btn-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Cambiar contraseña
                    </a>                        
                    </p>
                    <div class="collapse" id="collapseExample">                        
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="InputPassword">Contraseña</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                <div class="col-sm-6">
                                    <label for="InputPassword">Confirmar contraseña</label>
                                        <input type="password" id="confirm_password" class="form-control" name="confirm_password">
                                    </div>
                                </div>
                            </div>                        
                    </div>
                    <!---->
                    <div class="form-group">
                        <button type="submit" class="btn btn-success m-r-10">Guardar</button>
                        <a href="{{ route('admin.user.index') }}" class="btn btn-dark">Cancelar</a>
                    </div>      
                    </div>
                    <!---->                                 
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection