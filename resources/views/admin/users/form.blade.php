@extends('layouts.app')

@section('style-custom')
    <link rel="stylesheet" href="{{ asset('public/css/error.css') }}">
@endsection

@section('script-custom')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(4000);
</script>
    <script src="{{ asset('public/assets/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('public/assets/libs/tinymce/tinymce.min.js') }}"></script>    
 
    <script type="text/javascript">
        $ (document).ready(function () {
            $ ("#frmUser").validate({
                rules: {
                    user_name: {
                        required: true,
                        minlength: 4
                    }, 
                    email: {
                        required: true,
                        email: true
                    },      
                    password: {
                        required: true,
                        minlength: 8
                    },              
                },
                messages: {
                    user_name: {
                        required: "Por favor ingrese el nombre del usuario",
                        minlength: "Como minimo este campo debe contener 4 caracteres"
                    }, 
                    email: {
                        required: "Por favor ingrese el email del usuario",
                        email: "Pro favor ingrese un email valido."
                    },    
                    password: {
                        required: "Por favor ingrese la contraseña del usuario."{}
                        minlength: "Por motivos de seguridad su contraseña no debe contener menos de 8 caracteres"
                    },               
                },
            });
        });
    </script>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        @include('flash::message')
        <div class="card card-body">
            <h4 class="card-title">Registrar Usuario</h4>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    {!!Form::open(['method' => 'POST', 'id' => 'frmUser', 'role' => 'form', 'route' => 'admin.user.create', 'enctype' => 'multipart/form-data'])!!}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail111">Nombre Usuario</label>
                        <input type="text" class="form-control" id="user_name" name="user_name" maxlength="100">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail12">Correo Electrónico</label>
                        <input type="email" class="form-control" id="email" name="email"  maxlength="100">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword11">Ruc</label>
                        <input type="text" class="form-control" id="ruc" name="ruc" placeholder="" maxlength="11">
                    </div>
                    <!--********************************************-->
                    <div class="form-group">
                        <label for="exampleInputEmail111">Tipo de Usuario</label>
                        <select class="custom-select" id="type_user" name="type_user">
                            <option>Seleccionar....</option>
                            <option value="0">Cliente</option>
                            <option value="1">Administrador</option>
                            <option value="2">Super-administrador</option>
                        </select>
                    </div>
                    <!--********************************************-->
                    <div class="form-group">
                        <label for="exampleInputPassword12">Contraseña</label>
                        <input type="password" class="form-control" name="password" id="password" maxlength="255">
                    </div>
                    <button type="submit" class="btn btn-success m-r-10">Guardar</button>
                    <a href="{{ route('admin.user.index') }}" class="btn btn-dark">Cancelar</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
