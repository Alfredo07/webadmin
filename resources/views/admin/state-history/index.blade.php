@extends('layouts.app')

@section('script-custom')
    <script src="{{ asset('public/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('public/dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
@endsection

@section('content')
    <!-- order table -->
    <div class="row">
        <div class="col-12">
            <br>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Historial</h4>
                    <h6 class="card-subtitle">En esta sección se hace seguimiento a los cambios de estado de las
                        incidencias y cuanto es la demora en dar respuesta a los clientes.</h6>
                    <div class="table-responsive">
                        <table id="default_order" class="table table-hover table-striped table-bordered display"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ID REGISTRO</th>
                                <th>USUARIO</th>
                                <th>FECHA REGISTRO</th>
                                <th>FECHA RECEPCIÓN</th>
                                <th>FECHA EVALUACIÓN</th>
                                <th>FECHA CERRADO</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($history as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->idregistro }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->date_register}}</td>
                                    <td>{{ $row->date_reception }}</td>
                                    <td>{{ $row->date_evaluation }}</td>
                                    <td>{{ $row->date_close}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
