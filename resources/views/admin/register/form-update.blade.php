@extends('layouts.app')
@section('style-custom')
    <link rel="stylesheet" type="text/css"
          href="{{ asset('public/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}">
@endsection
@section('script-custom')
    <script src="{{ asset('public/assets/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#frmCreate").validate({
                rules: {
                    product: {
                        required: true,
                        minlength: 3
                    },
                    price: {
                        maxlength: 5,
                        number: true,
                        //digits: true
                    },
                    description: {
                        required: 9,
                        minlength: true
                    },
                },
                messages: {
                    product: {
                        required: "Por favor ingrese el nombre del product",
                        minlength: "Como minimo este campo debe contener 3 caracteres"
                    },
                    price: {
                        maxlength: "Por favor ingrese la dirección de la empresa",
                        number: "Solo puede contener números"
                    },
                    description: {
                        required: "Este campo solo puede contener 9 digitos",
                        minlength: "Solo puede contener numeros"
                    },
                },
            });
        });

    </script>
@endsection
@section('content')
    <form class="m-t-30" method="POST" enctype='multipart/form-data' files="true"
          ACTION="{{ route('admin.register.update') }}">
        <div class="card-body">
            <div class="col-12">
                <div class="card">
                    @if($dataRegister)
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="card-title">Cliente: {{ Auth::user()->name }}</h4>
                                    <h6 class="card-subtitle">Detalle de {{ $dataRegister->tipo }}</h6>
                                </div>
                            </div>
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $dataRegister->idregistro }}">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputEmail1">Asunto</label>
                                        <input type="text" class="form-control" name="caso" readonly
                                               value="{{  $dataRegister->caso }}">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputPassword1">Tipo</label>
                                        <input type="text" class="form-control" name="tipo" readonly
                                               value="{{ $dataRegister->tipo }}">
                                    </div>
                                </div>
                                @if(($dataRegister->tipo != 'Consulta') && ($dataRegister->tipo != 'Sugerencia') && ($dataRegister->afectado == 'Producto'))
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="exampleInputPassword1">Orden de Compra</label>
                                            <input type="text" class="form-control" name="ocompra"
                                                   value="{{ $dataRegister->ocompra }}" readonly>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="tipo">Orden Producción</label>
                                            <input type="text" name="opnum" class="form-control"
                                                   value="{{ $dataRegister->opnum }}" readonly>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputPassword1">Contacto</label>
                                        <input type="text" class="form-control" name="contacto"
                                               value="{{ $dataRegister->contacto }}" readonly>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="tipo">Cargo contacto</label>
                                        <input type="text" name="cargo_contacto" class="form-control"
                                               value="{{ $dataRegister->cargo_contacto }}" readonly>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="tipo">Correo contacto</label>
                                        <input type="text" name="email_contacto" class="form-control"
                                               value="{{ $dataRegister->email_contacto }}" readonly>
                                    </div>
                                </div>
                                @if(($dataRegister->tipo != 'Consulta') && ($dataRegister->tipo != 'Sugerencia') && ($dataRegister->afectado != "Servicio"))
                                    <div class="form-group">
                                        <label for="productotexto">Descripcion de {{ $dataRegister->tipo }}</label>
                                        <textarea type="text" rows="4" cols="50" name="productotexto"
                                                  class="form-control input-sm"
                                                  readonly>{{ $dataRegister->productotexto }}</textarea>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="detalle">Detalle de {{ $dataRegister->tipo }}</label>
                                    <textarea type="text" rows="4" cols="50" name="detalle" class="form-control"
                                              readonly>{{ $dataRegister->detalle }}</textarea>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="procede">Codigo SPV</label>
                                        <input type="text" name="spvnum" id="spvnum"
                                               value="{{ ($dataRegister->spvnum) ? $dataRegister->spvnum : ''}}"
                                               class="form-control input-sm" {{ ($dataRegister->spvnum) ? 'readonly'  : ''}}>
                                    </div>
                                                                
                                @if($dataRegister->tipo == "Reclamo")
                                    @if(!is_null($dataRegister->devolucion))
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="devolucion">Devolucion</label>
                                                <input type="text" class="form-control" readonly name="devolucion" id="devolucion" value="{{ $dataRegister->devolucion }}">
                                            </div>
                                        </div>
                                    @endif
                                </div>    
                                    @if(!is_null($dataRegister->procede))
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="procede">Procede</label>
                                                <select name="procede" id="procede" class="form-control input-sm"
                                                        disabled>
                                                    <option value="{{ $dataRegister->procede }}">{{ $dataRegister->procede }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="procede">Procede</label>
                                                <select name="procede" id="procede"
                                                        class="form-control input-sm">
                                                    <option value="" selected>Seleccionar...</option>
                                                    <option value="NO">NO</option>
                                                    <option value="SI">SI</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                @endif  
                        </div>
                    @endif    
                </div>
            </div>
        </div>
        <!--Panel-->
        <div class="card-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="card-title">Cliente: {{ Auth::user()->name }}</h4>
                                <h6 class="card-subtitle">Evaluación y respuesta</h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="evaluacion">Evaluación </label>
                            <textarea rows="4" cols="50" name="evaluacion" id="evaluacion"
                                      class="form-control">{{ ($dataRegister->evaluacion) ? $dataRegister->evaluacion : ''  }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="rutaspv">Adjuntar Documentos</label>
                            <input type="file" name="rutaspv" id="rutaspv" class="form-control input-sm">
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-success">Actualizar</button>
                                </div>
                            </div>
                            <div class="form-group col-sm-1">
                                <a href="{{ route('home') }}"><span class="btn btn-dark">Cancelar</span></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!---->
@endsection
