@extends('layouts.app')

@section('style-custom')
    <link rel="stylesheet" href="{{ asset('public/css/error.css') }}">
@endsection

@section('script-custom')

    <script src="{{ asset('public/assets/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('public/assets/libs/tinymce/tinymce.min.js') }}"></script>    
 
    <script type="text/javascript">
        $ (document).ready(function () {
            $ ("#frmCreate").validate({

                rules: {
                    caso: {
                        required: true,
                        minlength: 5
                    },
                    ocompra: {
                        required: true,
                        minlength: 1
                    },
                    opnum: {
                        required: true,
                        minlength: 9
                        //maxlength: 9,
                        //digits: true
                    },
                    contacto: {
                        required: true,
                        minlength: 4
                    },
                    cargo_contacto: {
                        required: true,
                        minlength: 4
                    },
                    email_contacto: {
                        required: true,
                        email: true
                    },
                    productotexto: {
                        required: true,
                        minlength: 5
                    },
                    detalle: {
                        required: true,
                        minlength: 5
                    },
                },
                messages: {
                    caso: {
                        required: "Por favor ingrese el asunto",
                        minlength: "Como minimo este campo debe contener 5 caracteres"
                    },
                    ocompra: {
                        required: "Por favor ingrese la orden de compra",
                        minlength: "Como mínimo este campo debe contener 4 caracteres"
                    },
                    opnum: {
                        required: "Por favor ingrese la orden de producción",
                        minlength: "Como mínimo este campo debe contener 9 caracteres"
                    },
                    contacto: {
                        required: "Por favor ingrese el nombre del contacto",
                        minlength: "Como mínimo este campo debe contener 4 caracteres"
                    },
                    cargo_contacto: {
                        required: "Por favor ingrese el cargo del contacto",
                        minlength: "Como mínimo este campo debe contener 4 caracteres"
                    },
                    email_contacto: {
                        required: "Por favor ingrese el email del contacto",
                        email: "Pro favor ingrese un email valido."
                    },
                    productotexto: {
                        required: "Por favor ingrese la descripción del producto",
                        minlength: "Como mínimo este campo debe contener 4 caracteres.",
                    },
                    detalle: {
                        required: "Por favor ingrese detalle de su reclamo",
                        minlength: "Como mínimo este campo debe contener 5 caracteres.",
                    },
                },
            });
        });

    </script>


    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })       

    </script>

    <script>
        function check()
        {
            var check_services = document.getElementById("customRadio2").checked = true;          
            console.log(check_services);
            if (check_services) {
                document.getElementById('orden_compra').style.display = 'none';
                document.getElementById('orden_produccion').style.display = 'none';
                document.getElementById('divDescripcion').style.display = 'none';
                document.getElementById('protocolo').style.display = 'none';
            } 
        }

        function unchecked(){
            var check_product = document.getElementById("customRadio1").checked = true;          
            console.log(check_product);
            if (check_product) {
                document.getElementById('orden_compra').style.display = 'block';
                document.getElementById('orden_produccion').style.display = 'block';
                document.getElementById('divDescripcion').style.display = 'block';
                document.getElementById('protocolo').style.display = 'block';
            } 
        }

        function mostrarPanel() {
            var tipo = document.getElementById('tipo').value;
            if (tipo == 'Reclamo') {
                document.getElementById('send').disabled = false;
                document.getElementById('chkBox').style.display = 'block';
                document.getElementById('protocolo').style.display = 'block'
                document.getElementById('divDetalle').style.display = 'block'
                document.getElementById('divDescripcion').style.display = 'block'
                document.getElementById('orden_compra').style.display = 'block';
                document.getElementById('orden_produccion').style.display = 'block';
                document.getElementById('rbtType').style.display = 'block';
                document.getElementById('lbl-detalle').innerHTML = "Detalle del " + tipo;
                document.getElementById('lbl-docajunto').innerHTML = "Adjunte sustento de su reclamo";

                //document.querySelectorAll('observacion, cons_tecnica ,sugerencia').style.display='none';
            } else if (tipo == 'Observacion') {
                /*ocultar*/
                document.getElementById('chkBox').style.display = 'none';
                /**/
                document.getElementById('protocolo').style.display = 'block'
                document.getElementById('divDetalle').style.display = 'block'
                document.getElementById('divDescripcion').style.display = 'block'
                document.getElementById('orden_compra').style.display = 'block';
                document.getElementById('orden_produccion').style.display = 'block';
                 document.getElementById('rbtType').style.display = 'block';
                document.getElementById('lbl-detalle').innerHTML = "Detalle de la " + tipo;
                document.getElementById('lbl-docajunto').innerHTML = "Adjunte sustento de su observación";
                document.getElementById('send').disabled = false;

                //document.querySelectorAll('reclamo, cons_tecnica ,sugerencia').style.display='none';
            } else if (tipo == 'Consulta') {
                /*ocultar*/
                document.getElementById('chkBox').style.display = 'none';
                document.getElementById('protocolo').style.display = 'none';                
                document.getElementById('divDescripcion').style.display = 'none';
                document.getElementById('orden_compra').style.display = 'none';
                document.getElementById('orden_produccion').style.display = 'none';
                 document.getElementById('rbtType').style.display = 'none';
                /**/

                document.getElementById('divDetalle').style.display = 'block'
                document.getElementById('lbl-detalle').innerHTML = "Detalle de la " + tipo;
                document.getElementById('send').disabled = false;
            } else if (tipo == 'Sugerencia') {
                /*ocultar*/
                document.getElementById('chkBox').style.display = 'none';
                document.getElementById('protocolo').style.display = 'none';
                document.getElementById('divDescripcion').style.display = 'none';
                document.getElementById('orden_compra').style.display = 'none';
                document.getElementById('orden_produccion').style.display = 'none';
                 document.getElementById('rbtType').style.display = 'none';
                /**/

                document.getElementById('divDetalle').style.display = 'block';
                document.getElementById('lbl-detalle').innerHTML = "Detalle de la " + tipo;
                document.getElementById('send').disabled = false;
            } else {
                document.getElementById('send').disabled = true;
                document.getElementById('chkBox').style.display = 'none';
                document.getElementById('protocolo').style.display = 'none';
                document.getElementById('divDetalle').style.display = 'none';
                document.getElementById('divDescripcion').style.display = 'none';
                document.getElementById('rbtType').style.display = 'none';
            }

        }
    </script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Nuevo Registro </h4>
                        <h6 class="card-subtitle">Primero debe seleccionar el tipo de incidencia que desea registrar.</h6>
                        <form class="m-t-30" id="frmCreate" method="POST" action="{{ route('admin.register.create') }}"
                              enctype='multipart/form-data' files="true" class="needs-validation">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="exampleInputEmail1">Tipo</label>
                                    <select class="custom-select" name="tipo" id="tipo" onchange="mostrarPanel()">
                                        <option selected="selected">Seleccionar...</option>
                                        <option value="Reclamo">Reclamo</option>
                                        <option value="Observacion">Observación</option>
                                        <option value="Consulta">Consulta Técnica</option>
                                        <option value="Sugerencia">Sugerencia</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="exampleInputEmail1">Asunto</label>
                                    <input type="text" class="form-control" name="caso" id="caso">
                                </div>                                
                            </div>
                                <!--Radio Buttons-->
                            <div class="row" id="rbtType" style="display: none">                    
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">                                
                                            <div class="custom-control custom-radio">
                                                <input type="radio" value="Producto" id="customRadio1" onclick="unchecked();" checked="checked" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Afecta a un producto</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" value="Servicio" id="customRadio2"  onclick="check();" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Afecta a un servicio</label>
                                            </div>                                
                                        </div>
                                    </div>
                                </div>
                            </div>                
                                <!---->
                            <div class="row">
                                <div class="col-sm-6" id="orden_compra">
                                    <div class="form-group">
                                        <label for="ocompra">Orden de Compra</label>
                                        <input type="text" name="ocompra" class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="col-sm-6" id="orden_produccion">
                                    <div class="form-group">
                                        <label for="tipo">Orden Producción</label>
                                        <input type="text" name="opnum" class="form-control input-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="tipo">Persona de Contacto<span class="text-danger">*</span></label>
                                        <input type="text" name="contacto" id="contacto" onkeypress="" maxlength="150" class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="tipo">Cargo del Contacto<span class="text-danger">*</span></label>
                                        <input type="text" name="cargo_contacto" id="cargo_contacto" onkeypress="" maxlength="150" class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="tipo">Correo del Contacto<span class="text-danger">*</span></label>
                                        <input type="text" name="email_contacto" id="email_contacto" maxlength="150" class="form-control input-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" id="divDescripcion" style="display:none">
                                    <div class="form-group">
                                        <label for="productotexto">Descripcion Producto</label>
                                        <textarea rows="1" name="productotexto" class="form-control input-sm"
                                                "></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12" id="divDetalle" style="display:none">
                                    <label for="detalle" id="lbl-detalle">Detalle</label>
                                    <textarea type="text" class="form-control" name="detalle" id="detalle"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="col-sm-12">
                                <div class=" col-sm-12 custom-control custom-checkbox" id="chkBox" style="display:none">
                                    <input type="checkbox" class="custom-control-input" cheked value="SI" type="checkbox" value="" name="devolucion"
                                           id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1" >Devolución</label>
                                </div>
                            </div>
                            <br>
                            
                            <br>
                            <div class="row">
                                <fieldset class="form-group col-sm-12" id="protocolo" style="display: none">
                                    <label for="docajunto" id="lbl-docajunto"><span></span></label>
                                    <input type="file" class="form-control-file" id="docajunto" name="docajunto">
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-1">
                                    <button type="submit" class="btn btn-success" id="send" disabled="true">Guardar</button>
                                </div>
                                <div class="form-group col-sm-1">
                                    <a href="{{ route('home') }}"><span class="btn btn-dark">Cancelar</span></a>
                                </div>
                            </div>
                        </form>
                        <!--
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Tinymce wysihtml5</h4>
                                            <h6 class="card-subtitle">Bootstrap html5 editor</h6>
                                            <form method="post">
                                                <textarea id="mymce" name="area"></textarea>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection