@extends('layouts.app')

@section('content')    
        @if($data_user)                   
            <div class="row">
                <div class="col-12">
                    <div class="col-4">     
                    <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Enviar comentario sobre la evaluación." target="_blank"><img src="{{ asset('public/assets/images/comentario.png') }}" data-toggle="modal" data-target="#responsive-modal" width="40" height="40"></a>               
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Detalle de {{ $data_user->tipo }}</h4>
                            <h6 class="card-subtitle">Este es el listado de todas las incidencias registradas por el usuario actual.</h6>
                            <form class="m-t-30" id="frmCreate" method="POST"
                                  action="{{ route('admin.register.create') }}" enctype='multipart/form-data'
                                  files="true" class="form-control">
                                     {{ csrf_field() }}
                                <div class="row">
                                
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputEmail1">Asunto</label>
                                        <input type="text" class="form-control" name="caso" id="caso" value="{{ $data_user->caso }}" readonly>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="exampleInputEmail1">Tipo</label>
                                        <input type="text" class="form-control" name="tipo" id="tipo" value="{{ $data_user->tipo }}" readonly>
                                    </div>
                                </div>
                                @if(($data_user->tipo != 'Consulta') && ($data_user->tipo != 'Sugerencia') && ($data_user->afectado == 'Producto'))
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="ocompra">Orden de Compra</label>
                                                <input type="text" name="ocompra" class="form-control input-sm" value="{{ $data_user->ocompra }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="tipo">Orden Producción</label>
                                                <input type="text" name="opnum" class="form-control input-sm" value="{{ $data_user->opnum }}" readonly>
                                            </div>
                                        </div>
                                    </div>  
                                @endif                              
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="ocompra">Contacto</label>
                                            <input type="text" name="ocompra" class="form-control input-sm" value="{{ $data_user->contacto }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="tipo">Cargo Contacto</label>
                                            <input type="text" name="opnum" class="form-control input-sm" value="{{ $data_user->cargo_contacto }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="ocompra">Correo Contacto</label>
                                            <input type="text" name="ocompra" class="form-control input-sm" value="{{ $data_user->email_contacto }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                @if(($data_user->tipo != 'Consulta') && ($data_user->tipo != 'Sugerencia') && ($data_user->afectado == "Producto"))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="productotexto">Descripcion del Producto</label>
                                                <textarea rows="1" name="productotexto" class="form-control input-sm" readonly>{{ $data_user->productotexto }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="detalle">Detalle de {{ $data_user->tipo }}</label>
                                        <textarea type="text" class="form-control" name="detalle" id="detalle" readonly>{{ $data_user->detalle }}</textarea>
                                    </div>
        @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>                
            </div>
            
    <!--Panel Respuesta-->   
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Evaluación</h4>
                        <h6 class="card-subtitle">En este módulo se podrá observar el resultado de la evaluación realizada asi como los documentos que corresponden.</h6>
                            <form class="m-t-30" id="frmCreate" enctype='multipart/form-data'
                                  files="true" class="form-control">
                                     {{ csrf_field() }}
                                @if($data_user->evaluacion)
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="detalle">Evaluación</label>
                                            <textarea type="text" rows="8"  class="form-control" name="detalle" id="detalle" readonly>{{ $data_user->evaluacion }}</textarea>
                                        </div>

                                    </div>

                                    <div class="row">
                                      @if($data_user->rutaspv)
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="detalle">Descargar Documentos</label><br/>
                                                <a target="_blank" href="{{asset('public/documentos/file-spv/'.$data_user->rutaspv)}}">{{ $data_user->rutaspv }}</a>                                       
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="detalle">Fecha Generado SPV</label>
                                                <input type="text" name="fechagenspv" class="form-control input-sm" value="{{$data_user->fechagenspv}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- sample modal content -->                
            <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">                                
                            <h4 class="modal-title">Ingrese su comentario</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('comment.create') }}">
                            {{ csrf_field() }}
                                <input type="hidden" name="id_registro" value="{{ $data_user->idregistro }}">
                                <input type="hidden" name="id_client" value="{{ auth()->id() }}">  
                                <input type="hidden" name="client" value="{{ $data_user->cliente }}">                                    
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Comentario:</label>
                                        <textarea required="true" class="form-control" id="message-text" name="comment" cols="40" rows="8"></textarea>
                                    </div>
                                
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light">Guardar</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
            <!-- /.modal -->   
@endsection
