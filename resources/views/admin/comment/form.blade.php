@extends('layouts.app')

@section('script-custom')
    <script>
        function deleteComment(id)
        {
            $.ajax({
                type: "GET",
                url: "{{ route('comment.delete') }}",
                data: { id: id},
                success: function(result) {
                    location.reload();
                },
                error: function() {
                    console.log("ERROR AL PROCESAR LA DATA");
                }
            });
        }
    </script>
@endsection

@section('content')
<div class="row">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Historial de Comentarios</h4>
            
            @if($comment_all)
                @foreach($comment_all as $row)
                <ul class="list-unstyled m-t-40">
                    <li class="media">
                        <img class="m-r-15" src="{{ asset('public/assets/images/list-comment.png') }}" width="40" alt="Generic placeholder image">
                        <div class="media-body">
                            <h5 class="mt-0 mb-1">{{ $row->client }} &nbsp;
                            <a href="#" onclick="deleteComment({{ $row->id }});"><i class="fas fa-trash-alt"></i></a>
                            <br><span class="card-subtitle">{{ $row->date_comment }}</span></h5>{{ $row->comment }}
                        </div>
                    </li>
                    <hr>
                    </ul>
                @endforeach               
            @endif    
            
        </div>
    </div>
</div>
@endsection